//Si no funciona, escribir en terminal: /etc/init.d/mysql start

let mysql = require("mysql");
let faker = require("faker");

let connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "demotech"
});

var data = [];
for (var i = 0; i < 500; i++) {
  data.push([faker.internet.email(), faker.date.past()]);
}

var q = "INSERT INTO users (email, created_at) VALUES ?";

connection.query(q, [data], function(err, result) {
  console.log(err);
  console.log(result);
});

connection.end();
