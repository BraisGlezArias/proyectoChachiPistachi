let express = require("express");
let mysql = require("mysql");
let bodyParser = require("body-parser");
let app = express();

app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));
let connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "demotech"
});

app.get("/", function(req, res) {
  let q = "SELECT COUNT(*) as count FROM users";
  connection.query(q, function(err, results) {
    if (err) throw err;
    let count = results[0].count;
    res.render("home", { data: count });
  });
});

app.post("/register", function(req, res) {
  let person = { email: req.body.email };
  connection.query("INSERT INTO users SET ?", person, function(err, result) {
    console.log(err);
    console.log(result);
    res.redirect("/");
  });
});

app.get("/joke", function(req, res) {
  let joke =
    "Three Database SQL walked into a NoSQL bar. A little while later they walked out... because they couldn't find a table.";
  res.send(joke);
});

app.listen(process.env.PORT || 3000, process.env.IP, () => {
  console.log("App listening on port 3000!");
});
