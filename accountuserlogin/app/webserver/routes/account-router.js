'use strict';

const express = require('express');
const createAccount = require('../controllers/account/create-account-controller');
const changePassword = require('../controllers/account/change-password-controller');
const changeUsername = require('../controllers/account/change-username-controller');


const router = express.Router();

router.post('/accounts', createAccount);
router.put('/accounts/password', checkAccountSession, changePassword);
router.put('/accounts/username', checkAccountSession, changeUsername);

module.exports = router;