'use strict';

const Joi = require('@hapi/joi');
const mysqlPool = require('../../../database/mysql-pool');

async function validateSchema(payload) {
  const schema = Joi.object({
    userName: Joi.string().trim().min(3).max(15).required({
      version: ['uuidv4'],
    }).required(),
    userId: Joi.string().guid({
      version: ['uuidv4'],
    }).required(),
  });

  Joi.assert(payload, schema);
}


async function changeUsername(req, res, next) {
  const { userId } = req.claims;

  try {
    await validateSchema(newUsername);
  } catch (e) {
    console.error(e);
    return res.status(400).send(e);
  }

  let connection;
  try {
    connection = await mysqlPool.getConnection();
    const now = new Date().toISOString().replace('T', ' ').substring(0, 19);
    
    const sqlUpdateNote = `UPDATE userName
      SET title = ?,
        content = ?
      WHERE id = ?
        AND user_id = ?`;

   
    await connection.query(sqlChangeUsername, [
      userId,
    ]);
    connection.release();

    return res.status(204).send();
  } catch (e) {
    if (connection) {
      connection.release();
    }

    console.error(e);
    return res.status(500).send({
      message: e.message,
    });
  }
}

module.exports = changeUsername;